package httpcli

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"time"
)

// HttpCli service to easy http communication
type HttpCli struct {
	Req      *http.Request
	Res      *http.Response
	Client   *http.Client
	ResBytes []byte
	Err      error
}

// New - initiates http request with specified method, url and body
func New(method, url string, body io.Reader) *HttpCli {
	hc := &HttpCli{
		Client: &http.Client{Timeout: 10 * time.Second},
	}

	hc.Req, hc.Err = http.NewRequest(method, url, body)
	return hc
}

// AddHeader - allows adding req headers
func (hc *HttpCli) AddHeader(key, val string) *HttpCli {
	if hc.Err != nil {
		return hc
	}

	hc.Req.Header.Set(key, val)
	return hc
}

// AddQueryParams adds query params to request url
func (hc *HttpCli) AddQueryParams(qp map[string]string) *HttpCli {
	q := url.Values{}

	for k, v := range qp {
		q.Add(k, v)
	}

	hc.Req.URL.RawQuery = q.Encode()
	return hc
}

// Do calls http client with defined request
func (hc *HttpCli) Do() *HttpCli {
	if hc.Err != nil {
		return hc
	}

	fmt.Printf("hc client: %v\n", hc.Req)
	hc.Res, hc.Err = hc.Client.Do(hc.Req)
	return hc
}

// Read - reads response body into slice of bytes
func (hc *HttpCli) Read() *HttpCli {
	if hc.Err != nil {
		return hc
	}

	hc.ResBytes, hc.Err = ioutil.ReadAll(hc.Res.Body)
	return hc
}

// Decode - decodes response body into given data structure
func (hc *HttpCli) Decode(data interface{}) *HttpCli {
	if hc.Err != nil {
		return hc
	}

	hc.Err = json.NewDecoder(hc.Res.Body).Decode(data)

	return hc
}

// HasError - checks response status code and error
func (hc *HttpCli) HasError() error {
	if hc.Err != nil {
		return hc.Err
	}

	if hc.Res.StatusCode >= http.StatusBadRequest {
		return fmt.Errorf("reuest failed with status:%d", hc.Res.StatusCode)
	}

	return nil
}
