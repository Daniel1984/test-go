## PHI ENV Reporter

### Prerequisites

1. you must set few `env` vars in gitlab:

- `PROJECT_IDS`
- `GITLAB_API_URL`
- `GITLAB_API_KEY`
- `SLACK_WEBHOOK_KEY`

2. Env var info

- `PROJECT_IDS` is a coma separated (string with no spaces), list of gitlab project ids. eg `"816,781"`
- `GITLAB_API_URL` by default should be `https://git.sussexdirectories.com/api/v4` unless different version is wanted.
- `GITLAB_API_KEY` should be created at `https://git.sussexdirectories.com/-/profile/personal_access_tokens` with full access
- `SLACK_WEBHOOK_KEY` - configuration steps:
