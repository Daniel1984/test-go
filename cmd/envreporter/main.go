package main

import (
	"fmt"
	"os"
	"strconv"
	"strings"

	"github.com/sirupsen/logrus"
	"github.com/xanzy/go-gitlab"
)

const (
	envGitApiKey     = "GITLAB_API_KEY"
	envGitApiUrlKey  = "GITLAB_API_URL"
	envProjectIDsKey = "PROJECT_IDS"
	// envSlackWebhookKey = "SLACK_WEBHOOK_KEY"
)

func main() {
	logger := logrus.New()
	logger.SetFormatter(&logrus.JSONFormatter{})

	gitlabApiKey, ok := os.LookupEnv(envGitApiKey)
	if !ok {
		logger.Fatalf("missing %s env var", envGitApiKey)
	}

	gitlabApiURL, ok := os.LookupEnv(envGitApiUrlKey)
	if !ok {
		logger.Fatalf("missing %s env var", envGitApiUrlKey)
	}

	gitlabProjectIDs, ok := os.LookupEnv(envProjectIDsKey)
	if !ok {
		logger.Fatalf("missing %s env var", envProjectIDsKey)
	}

	// slackWebhook, ok := os.LookupEnv(envSlackWebhookKey)
	// if !ok {
	// 	logger.Fatalf("missing %s env var", envSlackWebhookKey)
	// }

	git, err := gitlab.NewClient(gitlabApiKey, gitlab.WithBaseURL(gitlabApiURL))
	if err != nil {
		logger.Fatalf("failed to create client: %s", err)
	}

	envFilter := EnvFilter{
		Specifications: []EnvFilterSpec{
			excludeDevDevelopEnvFilter,
			qaEnvFilter,
			prodEnvFilter,
		},
	}

	msg := strings.Builder{}
	msg.WriteString("*Stale Environments Report*\n\n")

	for _, pID := range strings.Split(gitlabProjectIDs, ",") {
		projectID, err := strconv.Atoi(pID)
		if err != nil {
			logger.Errorf("converting project id from string: %d to int, err: %s", projectID, err)
			continue
		}

		proj, _, err := git.Projects.GetProject(projectID, nil)
		if err != nil {
			logger.Errorf("failed to get project: %d, err: %s", projectID, err)
			continue
		}

		msg.WriteString(fmt.Sprintf("*Project:* %s\n", proj.NameWithNamespace))
		msg.WriteString(fmt.Sprintf("*Project URL:* %s\n\n", proj.Namespace.WebURL))

		envs, _, err := git.Environments.ListEnvironments(projectID, &gitlab.ListEnvironmentsOptions{
			ListOptions: gitlab.ListOptions{Page: 0, PerPage: 100},
			States:      gitlab.String("available"),
		})
		if err != nil {
			logger.Errorf("failed fetching environments for project: %d, err: %s", projectID, err)
			continue
		}

		envs = envFilter.Filter(envs)

		if len(envs) == 0 {
			msg.WriteString("no stale environments found\n")
			msg.WriteString("-------------------------------------------\n\n")
			continue
		}

		msg.WriteString("*Stale environments:*\n\n")
		for i, env := range envs {
			msg.WriteString(fmt.Sprintf("*%d:*\n", i+1))
			msg.WriteString(fmt.Sprintf("\t*id:* %d\n", env.ID))
			msg.WriteString(fmt.Sprintf("\t*name:* %s\n", env.Name))
			msg.WriteString(fmt.Sprintf("\t*slug:* %s\n", env.Slug))
			msg.WriteString(fmt.Sprintf("\t*state:* %s\n", env.State))
			msg.WriteString(fmt.Sprintf("\t*created:* %s\n", env.CreatedAt))
		}
		msg.WriteString("-------------------------------------------\n\n")
	}

	msg.WriteString("*Make sure to delete all stale environments along with any deployed AWS resources*\n")
	fmt.Println(msg.String())
	os.Exit(0)
}

// EnvFilterSpec gitlab environment resource filter specification type
type EnvFilterSpec func(*gitlab.Environment) bool

// below specifying all the filters, more can be added if needed
func excludeDevDevelopEnvFilter(env *gitlab.Environment) bool {
	return env.Name != "dev-develop"
}

func qaEnvFilter(env *gitlab.Environment) bool {
	return !strings.HasPrefix(env.Name, "qa")
}

func prodEnvFilter(env *gitlab.Environment) bool {
	return !strings.HasPrefix(env.Name, "prod")
}

// EnvFilterSpec gitlab environment resource filter used as OCP + Specification pattern
type EnvFilter struct {
	Specifications []EnvFilterSpec
}

// Filter filters out unvanted gitlab environments dependong on specification
func (ef EnvFilter) Filter(envs []*gitlab.Environment) []*gitlab.Environment {
	filteredEnvs := make([]*gitlab.Environment, 0)
	for _, env := range envs {
		if ef.SpecMet(env) {
			filteredEnvs = append(filteredEnvs, env)
		}
	}
	return filteredEnvs
}

// SpecMet runs through list of filter specs and returns true only if all specs pass
func (ef EnvFilter) SpecMet(env *gitlab.Environment) bool {
	for _, specPass := range ef.Specifications {
		if !specPass(env) {
			return false
		}
	}

	return true
}
